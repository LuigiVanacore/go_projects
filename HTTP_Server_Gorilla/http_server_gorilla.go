package main

import (
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
)

const (
	CONN_PORT = ":8080"
)

type Person struct {
	Id string
	Name string
}

func renderTemplate(w http.ResponseWriter, r *http.Request) {
	person := Person{ "1", "Foo"}
	parsedTemplate, err := template.ParseFiles("templates/first_template.gohtml")
	if err != nil {
		log.Printf("Error occured while parsing the template: ", err)
		return
	}
	err = parsedTemplate.Execute(w, person)
	if err != nil {
		log.Printf("Error occured while executing the template or writing its output: ", err)
		return
	}
}
var GetRequestHandler = http.HandlerFunc(
	func(w http.ResponseWriter, r *http.Request) {
		helloWorld(w, r)
	})

var PostRequestHandler = http.HandlerFunc(
	func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("It's a Post Request"))
	})

var PathVariableHandler = http.HandlerFunc(
	func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		name := vars["name"]
		w.Write([]byte("Hi " + name))
	})

func helloWorld(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "Hello World!")
}

func main() {
	router := mux.NewRouter()
	router.Handle("/", handlers.LoggingHandler(os.Stdout, http.HandlerFunc(renderTemplate)))
	logfile, err := os.OpenFile("server.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatal("error file server.log: ", err)
	}
	router.PathPrefix("/").Handler(http.StripPrefix("/static",http.FileServer(http.Dir("static/"))))
	router.Handle("/post", handlers.LoggingHandler(logfile, PostRequestHandler)).Methods("POST")
	router.Handle("/hello/{name}", handlers.CombinedLoggingHandler(logfile, PathVariableHandler)).Methods("GET")
	err = http.ListenAndServe("localhost"+CONN_PORT, router)
	if err != nil {
		log.Fatal("error starting http server : ", err)
		return
	}
}
