package singleton

import "testing"



func TestGetInstance(t *testing.T) {
	counter := GetInstance()

	if counter == nil {
		t.Error("GetInstace: expected pointer to Singleton after calling GetInstance(), not nil \n")
	}

	expextedCounter := counter

	totalCount := counter.GetCount()
	if totalCount != 0 {
		t.Errorf("GetCount: The count must be 0 but it is %d \n", totalCount)
	}

	currentCount := counter.AddOne()
	if currentCount != 1 {
		t.Errorf("AddOne: After calling for the first time AddOne, the count must be 1 but it is %d \n", currentCount)
	}



	counter2 := GetInstance()
	if counter2 != expextedCounter {
		t.Error("Expexted same instance but in counter2 it got a different instance")
	}


	currentCount = counter2.AddOne()
	if currentCount != 2 {
		t.Errorf("AddOne: After calling AddOne for the second time, the count must be 2 but it is %d \n", currentCount)
	}

}
