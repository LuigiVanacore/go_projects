package main

import (
	"fmt"
	"github.com/asaskevich/govalidator"
	"github.com/gorilla/schema"
	"html/template"
	"log"
	"net/http"
	"io"
	"os"
)

//Constants for the server
const (
	CONN_PORT      = ":8080"
	ADMIN_USER     = "admin"
	ADMIN_PASSWORD = "admin"
	USERNAME_ERROR_MESSAGE = "Please enter a valid username"
	PASSWORD_ERROR_MESSAGE = "Please enter a valid password"
	GENERIC_ERROR_MESSAGE = "Validator error"
)

type User struct {
	Id   string
	Name string
	Username string `valid:"alpha,required"`
	Password string `valid:"alpha,required"`
}


//Handler functions

func readForm(r *http.Request) *User {
	r.ParseForm()
	user := new(User)
	decoder := schema.NewDecoder()
	decodeErr := decoder.Decode(user, r.PostForm)
	if decodeErr != nil {
		log.Printf("error mapping parsed form data to struct: ", decodeErr)
	}
	return user
}

func renderTemplate(w http.ResponseWriter, r *http.Request) {
	person := User{"1", "Foo", "Bar", "12345"}
	parsedTemplate, err := template.ParseFiles("templates/first_template.gohtml")
	if err != nil {
		log.Printf("Error occurred while parsing template file: ", err)
	}
	err = parsedTemplate.Execute(w, person)
	if err != nil {
		log.Printf("Error occurred while executing template: ", err)
		return
	}
}

func helloWorld(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello World!")
}

func login(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {

		parsedTemplate, err := template.ParseFiles("templates/login.gohtml")
		if err != nil {
			log.Printf("Error occurred while parsing template file: ", err)
		}
		parsedTemplate.Execute(w, nil)
	} else {
		user := readForm(r)
		valid, validationErrorMessage := validateUser(w, r, user)
		if !valid {
			fmt.Fprint(w, validationErrorMessage)
			return
		}
		fmt.Fprint(w, "Hello" + user.Username)
	}
}

func logout(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Logout page")
}

func  fileHandler(w http.ResponseWriter, r *http.Request)  {
	file, header, err := r.FormFile("file")
	if err != nil {
		log.Printf("error getting a file for the provided form key: ", err)
		return
	}
	defer file.Close()
	out, pathError := os.Create("tmp/uploadedFile")
	if pathError != nil {
		log.Printf("error creating a file for writing: ", pathError)
		return
	}
	defer out.Close()
	_, copyFileError := io.Copy(out, file)
	if copyFileError != nil {
		log.Printf("error occurred while file copy: ", copyFileError)
		return
	}
	fmt.Fprintf( w, "File uploaded successfully: ", header.Filename)
}

//Authentication handler
func validateUser(w http.ResponseWriter, r *http.Request, user *User) (bool, string) {
	valid, validationError := govalidator.ValidateStruct(user)
	if !valid {
		usernameError := govalidator.ErrorByField(validationError, "Username")
		passwordError := govalidator.ErrorByField(validationError, "Password")
		if usernameError != "" {
			log.Printf("username validation error: ", usernameError)
			return valid, USERNAME_ERROR_MESSAGE
		}
		if passwordError != "" {
			log.Printf("password validation error: ", passwordError)
			return valid, PASSWORD_ERROR_MESSAGE
		}
	}
	return valid, GENERIC_ERROR_MESSAGE
}

func main() {
	fileServer := http.FileServer(http.Dir("static"))
	http.Handle("/static/", http.StripPrefix("/static/", fileServer))
	//http.HandleFunc("/", BasicAuth(renderTemplate, "Please enter your username and password"))
	http.HandleFunc("/", login)
	http.HandleFunc("/login", login)
	http.HandleFunc("/logout", logout)
	err := http.ListenAndServe(CONN_PORT, nil)
	if err != nil {
		log.Fatal("error starting http server : ", err)
		return
	}
}
